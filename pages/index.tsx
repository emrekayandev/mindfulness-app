import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Mindfulness App</title>
        <meta name="description" content="Let's take a breath together!" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Let us take a breath
        </h1>

        <p className={styles.description}>
          Unwind with the 4-7-8 breathing method!
        </p>

        <div className={styles.grid}>
          <div className={styles.container}>
            <button className={styles.button}>Start the breathwork</button>
          </div>
        </div>
      
      </main>

      <footer className={styles.footer}>
        <p>Emre Kayan - 2022</p>
      </footer>
    </div>
  )
}
